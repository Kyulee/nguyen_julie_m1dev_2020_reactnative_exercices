import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import Homepage from "./views/Homepage";
import Navbar from "./views/layout/Navbar";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <Navbar />
      <Homepage />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: 30,
  },
});
