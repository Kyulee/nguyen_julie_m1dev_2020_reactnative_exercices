import React, { useState } from "react";
import { View, StyleSheet, TouchableOpacity, Text } from "react-native";

function ButtonCustom({ text, action }) {
  return (
    <TouchableOpacity
      style={styles.button}
      onPress={() => {
        action();
      }}
    >
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    marginTop: 10,

    backgroundColor: "pink",
    justifyContent: "center",

    borderRadius: 5,
    width: 150,
    height: 30,
  },
  text: {
    textAlign: "center",
  },
});

export default ButtonCustom;
