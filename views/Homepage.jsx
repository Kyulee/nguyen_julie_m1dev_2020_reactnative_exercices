import React from "react";
import { StyleSheet, View } from "react-native";
import ListExerciceView from "./ListExerciceView";
import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import ExerciceOne from "./ExercicesView/Exercice1";
import ExerciceTwo from "./ExercicesView/Exercice2";
import ExerciceThree from "./ExercicesView/Exercice3";
import ExerciceFour from "./ExercicesView/Exercice4";
import ExerciceFive from "./ExercicesView/Exercice5";
import ExerciceSix from "./ExercicesView/Exercice6";
import ExerciceSeven from "./ExercicesView/Exercice7";
import ExerciceEight from "./ExercicesView/Exercice8";
import ExerciceNine from "./ExercicesView/Exercice9";
import ExerciceTen from "./ExercicesView/Exercice10";
import ExerciceEleven from "./ExercicesView/Exercice11";
import ExerciceTwelve from "./ExercicesView/Exercice12";

const Stack = createStackNavigator();

function Homepage() {
  return (
    <View style={styles.container}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Liste d'exercices">
          <Stack.Screen name="Liste d'exercices" component={ListExerciceView} />
          <Stack.Screen name="Exercice 1" component={ExerciceOne} />
          <Stack.Screen name="Exercice 2" component={ExerciceTwo} />
          <Stack.Screen name="Exercice 3" component={ExerciceThree} />
          <Stack.Screen name="Exercice 4" component={ExerciceFour} />
          <Stack.Screen name="Exercice 5" component={ExerciceFive} />
          <Stack.Screen name="Exercice 6" component={ExerciceSix} />
          <Stack.Screen name="Exercice 7" component={ExerciceSeven} />
          <Stack.Screen name="Exercice 8" component={ExerciceEight} />
          <Stack.Screen name="Exercice 9" component={ExerciceNine} />
          <Stack.Screen name="Exercice 10" component={ExerciceTen} />
          <Stack.Screen name="Exercice 11" component={ExerciceEleven} />
          <Stack.Screen name="Exercice 12" component={ExerciceTwelve} />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});

export default Homepage;
