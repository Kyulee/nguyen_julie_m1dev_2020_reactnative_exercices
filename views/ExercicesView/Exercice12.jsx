import React, { useState, useEffect } from "react";
import { View, StyleSheet, Text } from "react-native";
import ButtonCustom from "../../components/ButtonCustom";

function ExerciceTwelve() {
  const [count, setCount] = useState(0);
  const onPress = () => setCount(count + 1);

  useEffect(() => {
    console.log("count: ", count);
    return () => {
      console.log("component will unmount");
    };
  }, [count]);

  return (
    <View style={styles.container}>
      <View style={styles.countContainer}>
        <Text>You've pressed the button: {count} time(s).</Text>
      </View>
      <ButtonCustom style={styles.button} action={onPress} text="Press me" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10,
    alignItems: "center",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
  },
  countContainer: {
    alignItems: "center",
    padding: 10,
  },
});

export default ExerciceTwelve;
