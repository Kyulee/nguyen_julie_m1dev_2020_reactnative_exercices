import React from "react";
import { StyleSheet, ScrollView } from "react-native";

import Square from "../../components/Square";

const getSquares = (nbSquare) => {
  const squares = [];
  for (let i = 0; i < nbSquare; i++) {
    squares[i] = "Square " + (i + 1);
  }
  return squares;
};

function ExerciceSix() {
  return (
    <ScrollView contentContainerStyle={styles.container}>
      {getSquares(15).map((square, index) => (
        <Square key={index} color="#f05d74" title={square} />
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 2000,
    flexGrow: 1,
    justifyContent: "space-between",
    alignItems: "center",
  },
});

export default ExerciceSix;
