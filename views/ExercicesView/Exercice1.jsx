import React from "react";
import { View, StyleSheet } from "react-native";
import Square from "../../components/Square";

function ExerciceOne() {
  return (
    <View style={styles.square}>
      <Square color="#7be1fa" title="Hello, world" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  square: {
    textAlign: "center",
    justifyContent: "center",
  },
});

export default ExerciceOne;
