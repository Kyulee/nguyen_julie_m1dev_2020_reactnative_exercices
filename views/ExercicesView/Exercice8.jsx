import React from "react";
import { View, StyleSheet, FlatList, Text, Image } from "react-native";

const fruits = [
  {
    id: 1,
    image:
      "https://www.vitabio.fr/img/modules/oh_ingredients/ingredients/8_picture-large.jpg",
    fruit: "Abricot",
  },
  {
    id: 2,
    image:
      "https://static.wikia.nocookie.net/mario/images/b/b1/MKWii-Banane.png/revision/latest?cb=20190531172142&path-prefix=fr",
    fruit: "Banane",
  },
  {
    id: 3,
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Cherry_Stella444.jpg/600px-Cherry_Stella444.jpg",
    fruit: "Cerise",
  },
  {
    id: 4,
    image:
      "https://www.pages.fr/cache/images/content/30e51b512a119dbc2b1d1e614a307ad4-citron-400x400-030718-42.png",
    fruit: "Citron",
  },
  {
    id: 5,
    image:
      "https://blog.meilisearch.com/content/images/2020/01/clementine_image.png",
    fruit: "Clementine",
  },
  {
    id: 6,
    image:
      "https://images.squarespace-cdn.com/content/v1/5bf9d707365f02cc1a66a32f/1584092481681-FMVBDLLWOTU8ED2WK0NU/ke17ZwdGBToddI8pDm48kO7Rg1Gpu728H4UqxUIfecJZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIARinE8o1oKw8ZdtQ1I7e9Mof-POsMkmuFchnzDEAptc/6-Strawberry-800x533.png",
    fruit: "Fraise",
  },
  {
    id: 7,
    image:
      "https://i0.wp.com/midwaybh.com/wp-content/uploads/2020/08/I-011664.png?fit=400%2C400&ssl=1",
    fruit: "Framboise",
  },
  {
    id: 8,
    image:
      "https://www.tang-freres.fr/wp-content/uploads/produits/fruits/fruit-du-dragon-pitaya.png",
    fruit: "Fruit du dragon",
  },
  {
    id: 9,
    image:
      "https://www.mpa-pro.fr/resize/500x500/zc/2/f/0/src/sites/mpapro/files/products/d11554.png",
    fruit: "Kiwi",
  },
  {
    id: 10,
    image:
      "https://cdn.pixabay.com/photo/2020/04/04/01/33/navel-5000527_960_720.png",
    fruit: "Orange",
  },
  {
    id: 11,
    image:
      "https://felpi.fr/wp-content/uploads/2018/12/poires-fruit-industrie.png",
    fruit: "Poire",
  },
  {
    id: 12,
    image: "https://felpi.fr/wp-content/uploads/2018/11/pomme-fuji-felpi-2.png",
    fruit: "Pomme",
  },
  {
    id: 13,
    image: "https://lesillonfruitsec.fr/wp-content/uploads/2014/08/Mangue.png",
    fruit: "Mangue",
  },
  {
    id: 14,
    image:
      "https://toutdunet.fr/wp-content/uploads/2020/06/Nectarine-1Kg-removebg-preview.png",
    fruit: "Nectarine",
  },
  {
    id: 15,
    image:
      "https://www.lamiedepain-boulangerie.fr/wp-content/uploads/2020/08/peche.png",
    fruit: "Pêche",
  },
];

function ExerciceEight() {
  return (
    <View style={styles.container}>
      <FlatList
        data={fruits}
        keyExtractor={({ id }, index) => id.toString()}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
            <Image source={{ uri: item.image }} style={styles.image} />
            <View style={styles.fruitName}>
              <Text style={{ fontWeight: "bold" }}>{item.fruit}</Text>
            </View>
          </View>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F7F7",
    marginTop: 10,
  },
  listItem: {
    margin: 10,
    padding: 10,
    backgroundColor: "#FFF",
    width: "80%",
    flex: 1,
    alignSelf: "center",
    flexDirection: "row",
    borderRadius: 5,
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  fruitName: {
    flex: 1,
    alignItems: "center",
  },
});

export default ExerciceEight;
