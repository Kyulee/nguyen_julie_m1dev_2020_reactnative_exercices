import React, { useState } from "react";
import { View, StyleSheet, Text, TextInput, Image } from "react-native";
import ButtonCustom from "../../components/ButtonCustom";

function SayHello(props) {
  return (
    <View style={styles.sayHelloContainer}>
      <Image
        style={styles.image}
        source={require("../../assets/helloCat.gif")}
      />
      <Text style={styles.textContainer}>Hello {props.who}</Text>
    </View>
  );
}

function ExerciceSeven() {
  const [name, setName] = useState("");
  const [saveName, setSaveName] = useState("");

  const displayText = () => {
    setSaveName(name);
  };

  return (
    <View style={styles.container}>
      <Text>What is your name ?</Text>
      <TextInput
        style={styles.input}
        value={name}
        onChangeText={(name) => setName(name)}
      />
      <ButtonCustom text="Say Hello" action={displayText} />

      {saveName !== "" && <SayHello who={saveName} />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 10,
    alignItems: "center",
  },
  input: {
    backgroundColor: "lightgrey",
    borderRadius: 5,
    width: 250,
    height: 35,
  },
  image: {
    width: 70,
    height: 70,
  },
  sayHelloContainer: {
    marginTop: 40,
    flexDirection: "row",
  },
  textContainer: {
    borderRadius: 15,
    backgroundColor: "pink",
    height: 30,
    width: 200,
    marginLeft: 10,
    borderBottomLeftRadius: 0,
    textAlign: "center",
    overflow: "hidden",
  },
});

export default ExerciceSeven;
