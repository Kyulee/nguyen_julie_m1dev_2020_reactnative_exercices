import React from "react";
import { View, StyleSheet, Text } from "react-native";

import Square from "../../components/Square";

function ExerciceFive() {
  return (
    <View style={styles.container}>
      <Square color="#7ce0fa" title="Square 1" />
      <Square color="#46b2b2" title="Square 2" />
      <Square color="#f05d74" title="Square 3" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
});

export default ExerciceFive;
