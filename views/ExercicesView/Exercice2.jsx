import React from "react";
import { View, StyleSheet, Button } from "react-native";
import ButtonCustom from "../../components/ButtonCustom";

function ExerciceTwo() {
  const tap = () => {
    alert("hello!");
  };
  return (
    <View style={styles.container}>
      <ButtonCustom text="Button 1" action={tap} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
});

export default ExerciceTwo;
