import React from "react";
import { View, StyleSheet } from "react-native";

import ButtonCustom from "../../components/ButtonCustom";

function ExerciceThree() {
  const alertTest = () => {
    alert("hello!");
  };
  return (
    <View style={styles.container}>
      <ButtonCustom text="Say Hello" action={alertTest} />
      <ButtonCustom text="Coucou Hibou" action={alertTest} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
});

export default ExerciceThree;
