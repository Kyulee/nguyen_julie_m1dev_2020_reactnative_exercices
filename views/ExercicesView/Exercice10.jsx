import React from "react";
import { View, StyleSheet } from "react-native";
import ButtonCustom from "../../components/ButtonCustom";

function ExerciceTen() {
  const alertTest = () => {
    alert("hello!");
  };
  return (
    <View style={styles.container}>
      <ButtonCustom text="Say Hello" action={alertTest} />
      <ButtonCustom text="Say Hello" action={alertTest} />
      <ButtonCustom text="Say Hello" action={alertTest} />
      <ButtonCustom text="Say Hello" action={alertTest} />
      <ButtonCustom text="Say Hello" action={alertTest} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
});

export default ExerciceTen;
