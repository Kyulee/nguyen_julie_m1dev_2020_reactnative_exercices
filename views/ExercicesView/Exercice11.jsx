import React from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import ButtonCustom from "../../components/ButtonCustom";

class ExerciceEleven extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.countContainer}>
          <Text>Press Count: {this.state.count}</Text>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.setState({ count: this.state.count + 1 })}
        >
          <Text>Press me</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10,
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
  },
  countContainer: {
    alignItems: "center",
    padding: 10,
  },
});

export default ExerciceEleven;
