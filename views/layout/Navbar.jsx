import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";

function Navbar() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require("../../assets/chatvabien.gif")}
      />
      <Text style={styles.title}>Exercice de test React Native</Text>
      <Text style={styles.title}>NGUYEN Julie</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: 50,
    height: 50,
  },
  title: {
    justifyContent: "center",
    textAlign: "center",
    alignItems: "center",
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default Navbar;
